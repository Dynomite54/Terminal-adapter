from subprocess import call
from sys import exit
killmelist = ['exit', 'exit()', 'quit', 'quit()', 'killme', 'die', 'kill']
def killme(x):
    print('')
    if str(x) in killmelist:
        print('dying...')
        exit()
    elif str(x) == 'help':
        print('This is a pretty self explanatory script.')
        print('Here are you killme words:')
        for r in killmelist:
            print(r)
while True:
    print('For a list of killme words, enter "help".')
    print('Enter a killme word to quit.')
    words = raw_input('Welcome to Terminal:\n')
    killme(words)
    splitwords = words.split()
    call(splitwords)
